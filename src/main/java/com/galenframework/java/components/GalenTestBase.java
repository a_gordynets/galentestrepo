package com.galenframework.java.components;

import com.galenframework.testng.GalenTestNgTestBase;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;

import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by a.gordynets on 06-Apr-17.
 */
public abstract class GalenTestBase extends GalenTestNgTestBase {

    private static final String ENV_URL = "https://money.lebara.com/uk/en/";

    @Override
    public WebDriver createDriver(Object[] args) {
        WebDriver driver = new ChromeDriver();
        if (args.length > 0) {
            if (args[0] != null && args[0] instanceof TestDevice) {
                TestDevice device = (TestDevice)args[0];
                if (device.getScreenSize() != null) {
                    driver.manage().window().setSize(device.getScreenSize());
                }
            }
        }
        return driver;
    }

    public void load(String url) {
        getDriver().get(url);
    }

    @DataProvider(name = "devices")
    public Object [][] devices () {
        return new Object[][] {
                {new TestDevice("Galaxy S5", new Dimension(360, 640), asList("Galaxy S5"))},
                {new TestDevice("Nexus 5X", new Dimension(412, 732), asList("Nexus 5X"))},
                {new TestDevice("Iphone 6 Plus", new Dimension(414, 736), asList("IPhone 6 Plus"))},
                {new TestDevice("Ipad Pro", new Dimension(1024, 1366), asList("Ipad Pro"))},
                {new TestDevice("desktop", new Dimension(1366, 1084), asList("desktop"))},
                {new TestDevice("android square", new Dimension(280, 280), asList("android square"))}
        };
    }

    @DataProvider(name = "desktop")
    public Object [][] desktop () {
        return new Object[][] {
                {new TestDevice("desktop", new Dimension(1936, 1056), asList("desktop"))},
        };
    }

    public static class TestDevice {
        private final String name;
        private final Dimension screenSize;
        private final List<String> tags;

        public TestDevice(String name, Dimension screenSize, List<String> tags) {
            this.name = name;
            this.screenSize = screenSize;
            this.tags = tags;
        }

        public String getName() {
            return name;
        }

        public Dimension getScreenSize() {
            return screenSize;
        }

        public List<String> getTags() {
            return tags;
        }

        @Override
        public String toString() {
            return String.format("%s %dx%d", name, screenSize.width, screenSize.height);
        }
    }
}

package tests;

import com.galenframework.java.components.GalenTestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by a.gordynets on 06-Apr-17.
 */
public class TestMainLebaraPage extends GalenTestBase{

    @Test(dataProvider = "devices")
    public void mainPageShouldLookOnDevice(TestDevice device) throws IOException, InterruptedException {
        load("https://money.lebara.com/uk/en/");
        checkLayout("/specs/homePageHeader.spec",device.getTags());
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);",
                getDriver().findElement(By.xpath("//*[@id='widget']")));
        Thread.sleep(1000);
        checkLayout("/specs/mainPageWidget.spec",device.getTags());
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);",
                getDriver().findElement(By.xpath("//*[@data-component-uid='PriceComparisonConfigurableImageTextComponentContainer']")));
        Thread.sleep(1000);
        checkLayout("/specs/homePageHowCompare.spec",device.getTags());
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);",
                getDriver().findElement(By.xpath("//*[@class='banner-component-01 homepage-text-component']")));
        Thread.sleep(1000);
        checkLayout("/specs/homePageTakeControl.spec",device.getTags());
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);",
                getDriver().findElement(By.xpath("//*[@class='homepage-widgets']//img[@alt='Mentioned Logos']")));
        Thread.sleep(1000);
        checkLayout("/specs/homePageVideoAndMention.spec",device.getTags());
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);",
                getDriver().findElement(By.xpath("//*[@class='main-header']")));
        Thread.sleep(2000);
        if (device.getName().equals("desktop") || device.getName().equals("Ipad Pro")){
            getDriver().findElement(By.xpath("//*[@class='liOffcanvas']/a")).click();
        } else {
            getDriver().findElement(By.xpath("//*[@class='navbar-toggle']")).click();
            Thread.sleep(1000);
            getDriver().findElement(By.xpath("//*[@class='liOffcanvas']/a")).click();
        }
        Thread.sleep(2000);
        checkLayout("/specs/loginPage.spec", device.getTags());
    }

    @Test
    public void mainPage(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://money.lebara.com/uk/en/");
        System.out.println(driver.findElement(By.xpath("//section[@class='row']//*[@class='banner-subheader']/p[2]")).getText());
        driver.quit();
    }

    @Test (dataProvider = "devices")
    public void NVidiaTextTest(TestDevice device) throws IOException {
        load("http://www.nvidia.ru/graphics-cards/geforce/pascal/gtx-1060/");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);",
                getDriver().findElement(By.xpath("//*[@class = 'ru-font' and contains(text()," +
                        "'И ВЫБЕРИ ИГРУ, КОТОРУЮ ТЫ ПОЛУЧИШЬ БЕСПЛАТНО.* ')]")));
        checkLayout("/specs/nvidiaPage.spec",device.getTags());
    }

    @Test (dataProvider = "desktop")
    public void TesthomePageQuickAndEasy(TestDevice device) throws IOException {
        load("https://money.lebara.com/uk/en/");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", getDriver().findElement(By.xpath("//*[contains(@class, 'row js-responsive-background-image') and .//a[contains(text(), 'Find out more')]]")));
        checkLayout("/specs/homePageQuickAndEasy.spec", device.getTags());
        getDriver().quit();
    }

    @Test (dataProvider = "desktop")
    public void homePageSaveWithLebara (TestDevice device) throws IOException {
        load("https://money.lebara.com/uk/en/");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", getDriver().findElement(By.xpath("//*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]")));
        checkLayout("/specs/homePageSaveWithLebara.spec", device.getTags());
        getDriver().quit();
    }

    @Test (dataProvider = "desktop")
    public void homePageSaveWidget (TestDevice device) throws IOException {
        load("https://money.lebara.com/uk/en/");
//        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", getDriver().findElement(By.xpath("//*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]")));
        checkLayout("/specs/homePageWidget.spec", device.getTags());
        getDriver().quit();
    }

    @Test (dataProvider = "desktop")
    public void HowDoesLebaraMoneyTransferWork(TestDevice device) throws IOException {
        load("https://money.lebara.com/uk/en/");
//        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", getDriver().findElement(By.xpath("//*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]")));
        getDriver().findElement(By.xpath("//*[@id='thumbnail-video']")).click();
        checkLayout("/specs/HowDoesLebaraMoneyTransferWork.spec", device.getTags());
        getDriver().quit();
    }


}

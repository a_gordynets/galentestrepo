@objects
#father
    click                                                                       xpath     //*[@id='thumbnail-video']

#desktop locators
    video-popup                                                                 xpath     //*[@class='popup-user video-popup']
    video-popup-title                                                           xpath     //*[@class='popup-user video-popup']/h1
    video-popup-iframe                                                          xpath     //*[@class='popup-user video-popup']/iframe
    video-popup-button                                                          xpath     //*[@class='popup-user video-popup']/a

= Video popup (chrome) =
    @on desktop
        video-*:
            visible
            centered horizontally inside screen 2 px

        video-popup:
            centered horizontally inside screen 1 px
            on top left edge screen 0 to 5 px bottom
            contains video-popup-*

        video-popup-title:
            on top left edge video-popup 65 to 75 px bottom
            css font-size is "35px"
            css font-family is "gotham-medium"

        video-popup-iframe:
            below video-popup-title 10 to 20 px
            height 395 to 405 px
            width 710 to 720 px

        video-popup-button:
            below video-popup-iframe 30 to 40 px
            height 40 to 50 px
            width 395 to 405 px
            css font-size is "18px"
            css font-family is "gotham-medium"


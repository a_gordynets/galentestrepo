
@objects
#common locators
    header                                    xpath     //*[@class='main-header']

#desktop locators
    header-menu-lang                          xpath     //*[@class='language-selector hidden-xs']//*[@class='dropdownheader hidden-xs']
    header-menu-lang-img                      xpath     //*[@class='language-selector hidden-xs']//*[@class='dropdownheader hidden-xs']/img
    header-menu-lang-arrow                    xpath     //*[@class='language-selector hidden-xs']//span[@class='arrow']
    header-menu-login                         xpath     //*[@class='liOffcanvas']
    header-menu-login-icon                    xpath     //*[@class='liOffcanvas']/span
    header-menu-login-icon-text               xpath     //*[@class='liOffcanvas']/a
    header-menu-secondary                     xpath     //*[@class='header-menu-secondary']
    header-menu-secondary-picture             xpath     //*[@class='header-menu-secondary']//*[@class='simple-banner-component']/a/img
    header-menu-secondary-link-*              xpath     //*[@class='header-menu-secondary']//*[@class='header-menu-right']/a

#mobile locators
    mobile-header-navbar                      xpath     //*[@class='navbar-toggle']
    mobile-header-site-logo                   xpath     //*[@class='site-logo-responsive row']




= Header (chrome) (1920x932 px) =
    @on *
        header:
            visible
            inside screen 0 px top
            width 95 to 100 % of screen/width
            centered horizontally inside screen 1 px

    @on desktop
        header:
            visible
            height 30 to 40 px
            contains header-menu-lang-img, header-menu-lang, header-menu-lang-arrow, header-menu-login, header-menu-login-*

        header-menu-lang:
            visible
            css font-size is "13px"
            css font-family is "gotham-book"
            on top left edge header 950 to 1050 px right, 10 to 14 px bottom
            contains header-menu-lang-*

        header-menu-lang-img:
            visible
            height 13 to 15 px
            width 18 to 24 px
            on top left edge header-menu-lang 0 to 10 px right, 0 to 10 px bottom

        header-menu-lang-arrow:
            visible
            on top left edge header 15 to 20 px bottom
            on top left edge header-menu-lang 80 to 90 px right, 0 to 10 px bottom

        header-menu-login:
            visible
            contains header-menu-login-*
            on top left edge header 5 to 10 px bottom
            right-of header-menu-lang-arrow 20 to 30 px

        header-menu-login-icon:
            visible
            height 15 to 19 px
            width 15 to 19 px
            right-of header-menu-lang-arrow 20 to 30 px

        header-menu-login-icon-text:
            visible
            css font-size is "13px"
            css font-family is "gotham-light"
            right-of header-menu-login-icon 0 to 10 px

        header-menu-secondary:
            visible
            height 95 to 105 px
            width 95 to 100 % of screen/width
            below header 0 to 5 px
            contains header-menu-secondary-*

        header-menu-secondary-picture:
            visible
            height 75 to 85 px
            width 145 to 155 px
            inside screen 100 to 150 px left
            below header 10 to 20 px


        @forEach [header-menu-secondary-link-*] as linkName, next as nextLink
            ${linkName}:
                left-of ${nextLink} 25 to 35px
                aligned horizontally top ${nextLink}
                css font-size is "16px"
                css font-family is "gotham-medium"

        mobile-*:
            absent




    @on Galaxy S5
        header:
            height ~ 55

    @on Nexus 5X
        header:
            height ~ 55

    @on Iphone 6 Plus
        header:
            height ~ 55


    @on Ipad Pro
        header:
            height ~ 48






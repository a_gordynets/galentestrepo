@objects
#upper element
    take-control-body                                                xpath     //*[@class='col-lg-12 col-md-12 col-sm-12 col-xs-12 hompepage-content']/div[1]

#common locators
    how-compare-body                                                 xpath     //*[@data-component-uid='PriceComparisonConfigurableImageTextComponentContainer']
    how-compare-body-section-header                                  xpath     //*[@data-component-uid='PriceComparisonConfigurableImageTextComponentContainer']//*[@class='h4 section-header']
    how-compare-body-section-sub-header                              xpath     //*[@data-component-uid='PriceComparisonConfigurableImageTextComponentContainer']//*[@class='h2 section-sub-header']
    how-compare-body-section-like-the-rate                           xpath     //*[@data-component-uid='PriceComparisonConfigurableImageTextComponentContainer']//*[@class='section-body-text like-the-rate']
    how-compare-body-image-content                                   xpath     //*[@data-component-uid='PriceComparisonConfigurableImageTextComponentContainer']//*[@class='image-content-component left top center']

#desktop locators

#mobile locators


= How Compare (chrome) =
    @on *
        how-compare-*:
            visible

        how-compare-body:
            width 90 to 100 % of screen/width
            below take-control-body 0 to 5 px

    @on desktop
        how-compare-body:
            contains how-compare-body-*
            height 670 to 690 px

        how-compare-body-section-header:
            centered horizontally inside screen 1 px
            on top left edge how-compare-body 75 to 85 px bottom
            css font-size is "13px"
            css font-family is "gotham-bold"

        how-compare-body-section-sub-header:
            centered horizontally inside screen 1 px
            below how-compare-body-section-header 15 to 25 px
            css font-size is "35px"
            css font-family is "gotham-bold"

        how-compare-body-section-like-the-rate:
            centered horizontally inside screen 1 px
            below how-compare-body-section-sub-header 0 to 20 px
            css font-size is "16px"
            css font-family is "gotham-book"

        how-compare-body-image-content:
            centered horizontally inside screen 1 px
            height 350 to 360 px
            width 1080 to 1100 px
            below how-compare-body-section-like-the-rate 20 to 30 px

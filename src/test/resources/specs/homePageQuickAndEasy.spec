@objects
#upper element
    reason-to-send                                                            xpath     //*[contains(@class, 'row js-responsive-background-image') and .//a[contains(text(), 'Get Started')] and .//iframe]

#common locators
    quick-and-easy                                                            xpath     //*[contains(@class, 'row js-responsive-background-image') and .//a[contains(text(), 'Find out more')]]
    quick-and-easy-text-component-section                                     xpath     //*[contains(@class, 'row js-responsive-background-image') and .//a[contains(text(), 'Find out more')]]//*[@class='text-component text-center']
    quick-and-easy-text-component-section-header                              xpath     //*[contains(@class, 'row js-responsive-background-image') and .//a[contains(text(), 'Find out more')]]//*[@class='h4 section-header']
    quick-and-easy-text-component-section-sub-header                          xpath     //*[contains(@class, 'row js-responsive-background-image') and .//a[contains(text(), 'Find out more')]]//*[@class='h2 section-sub-header']
    quick-and-easy-img                                                        xpath     //*[contains(@class, 'row js-responsive-background-image') and .//a[contains(text(), 'Find out more')]]//*[@class='js-responsive-image img-responsive']
    quick-and-easy-text-component-section-button                              xpath     //*[contains(@class, 'row js-responsive-background-image') and .//a[contains(text(), 'Find out more')]]//*[@class='btn ']

#desktop locators

#mobile locators


= Quick and easy (chrome) =
    @on *
        quick-and-*:
            visible

        quick-and-easy:
            contains quick-and-easy-*
            width 95 to 100 % of screen/width
            below reason-to-send 0 to 5 px

    @on desktop
        quick-and-easy:
            height 520 to 530 px

        quick-and-easy-text-component-section:
            contains quick-and-easy-text-component-section-*
            on top right edge quick-and-easy 75 to 85 px bottom

        quick-and-easy-text-component-section-header:
            on top right edge quick-and-easy-text-component-section 0 to 10 px bottom
            css font-size is "13px"
            css font-family is "gotham-bold"

        quick-and-easy-text-component-section-sub-header:
            below quick-and-easy-text-component-section-header 15 to 25 px
            css font-size is "35px"
            css font-family is "gotham-bold"

        quick-and-easy-img:
            centered horizontally inside screen 1 px
            below quick-and-easy-text-component-section-sub-header 25 to 35 px
            height 185 to 190 px
            width 795 to 800 px

        quick-and-easy-text-component-section-button:
            below quick-and-easy-img 35 to 45 px
            css font-size is "14px"
            css font-family is "gotham-medium"

        quick-and-easy-text-component-section-*:
            centered horizontally inside screen 2 px

@objects
#upper element
    quick-and-easy                                                             xpath     //*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Quick and easy')]]

#common locators
    save-with-lebara                                                           xpath     //*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]
    save-with-lebara-img                                                       xpath     //*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]//*[@class='js-responsive-image img-responsive']
    save-with-lebara-text-component-section                                    xpath     //*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]//*[@class='text-component right bottom']
    save-with-lebara-text-component-section-header                             xpath     //*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]//*[@class='h4 section-header']
    save-with-lebara-text-component-section-sub-header                         xpath     //*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]//*[@class='h2 section-sub-header']
    save-with-lebara-text-component-section-text                               xpath     //*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]//*[@class='section-body-text like-the-rate']
    save-with-lebara-text-component-section-button                             xpath     //*[contains(@class, 'row js-responsive-background-image') and .//h2[contains(text(), 'Save with lebara money transfer')]]//*[@class='btn  js-custom-pupup']

#desktop locators

#mobile locators

= Save with lebara (chrome) =
    @on *
        save-with-*:
            visible

        save-with-lebara:
            contains save-with-lebara-*
            width 95 to 100 % of screen/width
            below quick-and-easy 0 to 5 px

    @on desktop
        save-with-lebara:
            height 410 to 420 px

        save-with-lebara-img:
            on top left edge save-with-lebara 65 to 75 px bottom, 410 to 420 px right
            height 270 to 280 px
            width 420 to 430 px

        save-with-lebara-text-component-section:
            contains save-with-lebara-text-component-section-*
            on top right edge save-with-lebara 75 to 85 px bottom, 930 to 945 px left

        save-with-lebara-text-component-section-header:
            on top right edge save-with-lebara-text-component-section 0 to 10 px bottom
            css font-size is "13px"
            css font-family is "gotham-bold"

        save-with-lebara-text-component-section-sub-header:
            below save-with-lebara-text-component-section-header 15 to 25 px
            css font-size is "35px"
            css font-family is "gotham-bold"

        save-with-lebara-text-component-section-text:
            below save-with-lebara-text-component-section-sub-header 5 to 15 px
            css font-size is "16px"
            css font-family is "gotham-book"

        save-with-lebara-text-component-section-button:
            below save-with-lebara-text-component-section-text 25 to 35 px
            css font-size is "14px"
            css font-family is "gotham-medium"

        save-with-lebara-text-component-section-*:
            aligned vertically left save-with-lebara-text-component-section-header

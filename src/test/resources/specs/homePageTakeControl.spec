@objects
#upper element
    header                                                                   xpath     //*[@class='main-header']

#common locators
    take-control                                                             xpath     //*[@class='banner-component-01 homepage-text-component']
    take-control-header                                                      xpath     //*[@class='banner-header']
    take-control-header-text                                                 xpath     //*[@class='banner-header']/h1
    take-control-sub-header                                                  xpath     //*[@class='banner-subheader']
    take-control-sub-header-text-*                                           xpath     //*[@class='banner-subheader']/p


#desktop locators



#mobile locators


= Take Control (chrome) =
    @on *
        take-control,take-control-header,take-control-sub-header:
            visible

    @on desktop
        take-control:
            contains take-control-*
            below header 145 to 155 px
            inside screen 430 to 450 px left

        take-control-header:
            contains take-control-header-*

        take-control-header-text:
            css font-size is "40px"
            css font-family is "gotham-medium"
            on top left edge take-control 0 to 10 px bottom

        take-control-sub-header:
            contains take-control-sub-header-*
            below take-control-header 20 to 30 px

        take-control-sub-header-text-1:
            on top left edge take-control-sub-header 0 to 10 px bottom

        @forEach [take-control-sub-header-text-*] as textName, next as nextText
            ${textName}:
                below ${nextText} 25 to 35px
                aligned vertically left ${nextText}
                css font-size is "16px"
                css font-family is "gotham-light"




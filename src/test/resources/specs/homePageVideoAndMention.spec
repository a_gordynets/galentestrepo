@objects

#TO DO check video on specific scenario

#upper element
    take-control                                                             xpath     //*[@class='banner-component-01 homepage-text-component']

#common locators
#    homepage-widget                                                          xpath     //*[@class='homepage-widgets']
    homepage-widget-img                                                      xpath     //*[@class='homepage-widgets']//img[@alt='Mentioned Logos']

#desktop locators
    video-component                                                          xpath     //*[@class='howitworks-video-component hidden-xs']
    video-component-img                                                      xpath     //*[@class='howitworks-video-component hidden-xs']//img
    video-component-text                                                     xpath     //*[@class='howitworks-video-component hidden-xs']//div[@class='video-text']

#mobile locators


= Video And Mention (chrome) =
    @on *
        homepage-widget-img:
            visible

    @on desktop
        video-component, video-component-*:
            visible

        video-component:
            contains video-component-*
            below take-control 145 to 155 px
            inside screen 430 to 450 px left

        video-component-img:
            height 45 to 55 px
            width 70 to 80 px
            inside video-component 400 to 450 px left, 0 to 10 px top

        video-component-text:
            css font-size is "13px"
            css font-family is "gotham-light"
            inside video-component 400 to 450 px left, 0 to 10 px top

        homepage-widget-img:
            height 55 to 65 px
            width 495 to 505 px
            inside video-component 400 to 450 px left, 0 to 10 px top

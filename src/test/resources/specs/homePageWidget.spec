@objects
#upper element for desktop
    header                                    xpath     //*[@class='main-header']


#common locators
    widget                                    xpath     //*[@id='transferForm']
    widget-header                             xpath     //*[@class='widget-header']
    widget-header-text-span-1                 xpath     //*[@class='widget-header']//span[@class='fxRateContent']
    widget-header-text-span-2                 xpath     //*[@class='widget-header']//span[@class='fxRateContent fx-display']
    widget-header-text-span-3                 xpath     //*[@class='widget-header']//span[@class='fxRateContent fx-currency']
    widget-body                               xpath     //*[@class='widget-body']
    widget-body-you-send                      xpath     //*[@class='widget-body']//*[@for='send']
    widget-body-you-send-number               xpath     //*[@class='widget-body']//*[@id='send']
    widget-body-you-send-flag                 xpath     //*[@class='send']//img
    widget-body-you-send-sell-currency        xpath     //*[@class='send']//*[@id='sellCurrency']
    widget-body-switch                        xpath     //*[@class='switch']
    widget-body-they-get                      xpath     //*[@class='widget-body']//*[@for='receive']
    widget-body-they-get-number               xpath     //*[@class='widget-body']//*[@id='receive']
    widget-body-they-get-flag                 xpath     //*[@class='widget-body']//div[@class='country-container']/img
    widget-body-they-get-buy-country          xpath     //*[@class='widget-body']//*[@id='country-display']
    widget-body-they-get-buy-country-arrow    xpath     //*[@class='widget-body']//*[@class='arrow']
    widget-body-send-money                    xpath     //*[@class='widget-body']//p[@class='label no-padding-left']
    widget-body-send-money-today-radio        xpath     //*[@class='widget-body']//*[@id='today']
    widget-body-send-money-today              xpath     //*[@class='widget-body']//*[@for='today']
#doesn't work  <--
#    widget-body-send-money-later-radio        xpath     //*[@class='widget-body']//*[@id='later']
    -->
    widget-body-send-money-later-label        xpath     //*[@class='widget-body']//*[@for='later']
#TO DO insert into tests <--
    widget-body-send-money-later-info         xpath     //*[@class='widget-body']//*[@class='info']
    widget-body-send-money-later-info-pink    xpath     //*[@class='widget-body']//*[@class='text-pink lock']
# -->
    widget-table-wrapper                      xpath     //*[@class='table-wrapper']
    widget-table-wrapper-fee                  xpath     //*[@class='table-wrapper']//*[@id='fee-heading']
    widget-table-wrapper-fee-currency         xpath     //*[@class='table-wrapper']//*[@class='fee']/span
    widget-table-wrapper-fee-value            xpath     //*[@class='table-wrapper']//*[@class='fee']/p
    widget-table-wrapper-zero-fee             xpath     //*[@class='discount text-pink bold']/td[contains(text(),'ZERO FEE OFFER')]
    widget-table-wrapper-zero-fee-currency    xpath     //*[@class='discount text-pink bold']//span
    widget-table-wrapper-zero-fee-value       xpath     //*[@class='discount text-pink bold']//p
    widget-table-wrapper-total                xpath     //*[@class='table-wrapper']//td[contains(text(),'Total')]
    widget-table-wrapper-total-currency       xpath     //*[@class='table-wrapper']//*[@class='total']/span
    widget-table-wrapper-total-value          xpath     //*[@class='table-wrapper']//*[@class='total']/p
    widgetbutton-get-started                  xpath     //*[@id='getStarted']
    widgetbutton-get-started-text             xpath     //*[@id='getStarted']/*[@id='content']



= Widget (chrome) =
    @on *
        widget:
            contains widget-*
            visible

        widget-*:
            visible

        widgetbutton-get-started:
            below widget 10 to 20 px
            aligned vertically all widget
            contains widgetbutton-get-started-text

        widgetbutton-*:
            visible


    @on desktop
        widget:
            inside screen 430 to 440 px right
            below header 100 to 120 px
            height 425 to 430 px
            width 355 to 365 px

# Header
        widget-header:
            height 55 to 65 px
            on top left edge widget 0 to 10 px right, 0 to 10 px bottom , 0 to 10 px left
            contains widget-header-text-span-*

        @forEach [widget-header-text-span-*] as spanName, next as nextSpan
            ${spanName}:
                left-of ${nextSpan} 0 to 15px
                aligned horizontally top ${nextSpan}
                css font-size is "24px"
                css font-family is "Gotham-Book"

# Body
        widget-body:
            below widget-header 0 to 5 px
            height 280 to 290 px
            on top left edge widget 0 to 10 px right, 0 to 10 px left
            contains widget-body-*

        widget-body-you-send:
            on top left edge widget-body 20 to 30 px right, 0 to 10 px bottom
            css font-size is "12px"
            css font-family is "Gotham-Book"

        widget-body-you-send-number:
            below widget-body-you-send 0 to 5 px
            css font-size is "26px"
            css font-family is "Gotham-Medium"
            aligned vertically left widget-body-you-send

        widget-body-you-send-flag:
            height 10 to 15 px
            width 15 to 20 px
            on top left edge widget-body 260 to 270 px right, 30 to 40 px bottom

        widget-body-you-send-sell-currency:
            on top left edge widget-body 20 to 30 px bottom
            right-of widget-body-you-send-flag -12 to -8 px
            css font-size is "16px"
            css font-family is "Gotham-Light"

        widget-body-switch:
            height 30 to 40 px
            width 30 to 40 px
            on top left edge widget-body 210 to 220 px right, 60 to 70 px bottom

        widget-body-they-get:
            below widget-body-you-send-number 10 to 20 px
            css font-size is "12px"
            css font-family is "Gotham-Book"
            aligned vertically left widget-body-you-send-number

        widget-body-they-get-number:
            below widget-body-they-get 0 to 5 px
            css font-size is "26px"
            css font-family is "Gotham-Medium"
            aligned vertically left widget-body-they-get

        widget-body-they-get-flag:
            height 10 to 17 px
            width 15 to 20 px
            below widget-body-you-send-flag 60 to 70 px
            aligned vertically left widget-body-you-send-flag 2px

        widget-body-they-get-buy-country:
            css font-size is "16px"
            css font-family is "Gotham-Light"
            below widget-body-you-send-sell-currency 45 to 50 px
            aligned vertically left widget-body-you-send-sell-currency 15px

        widget-body-they-get-buy-country-arrow:
            height 5 to 7 px
            width 7 to 11 px
            below widget-body-they-get 10 to 15 px
            right-of widget-body-they-get-buy-country 3 to 6 px

        widget-body-send-money:
            below widget-body-they-get-number 10 to 20 px
            css font-size is "12px"
            css font-family is "gotham-book"
            aligned vertically left widget-body-they-get-number

        widget-body-send-money-today-radio:
            height 5 to 7 px
            width 7 to 11 px
            below widget-body-send-money 10 to 15 px
            aligned vertically left widget-body-send-money

        widget-body-send-money-today:
            below widget-body-send-money 10 to 15 px
            right-of widget-body-send-money-today-radio 5 to 15 px
            css font-size is "18px"
            css font-family is "Gotham-Medium"

#        widget-body-send-money-later-radio:
#            height 5 to 7 px
#            width 7 to 11 px
#            below widget-body-send-money-today-radio 10 to 15 px
#            aligned vertically left widget-body-send-money-today-radio

        widget-body-send-money-later-label:
            below widget-body-send-money-today 10 to 15 px
            aligned vertically left widget-body-send-money-today
            css font-size is "18px"
            css font-family is "Gotham-Medium"

# Wrapper
        widget-table-wrapper:
            below widget-body 0 to 5 px
            height 80 to 90 px
            on top left edge widget-body 0 to 10 px right, 0 to 10 px left
            contains widget-table-wrapper-*

        widget-table-wrapper-fee:
            on top left edge widget-table-wrapper 20 to 30 px left, 0 to 10 px bottom
            css font-size is "18px"
            css font-family is "Gotham-Medium"

        widget-table-wrapper-fee-currency:
            aligned horizontally bottom widget-table-wrapper-fee
            right-of widget-table-wrapper-fee 5 to 10 px
            css font-size is "18px"
            css font-family is "Gotham-Medium"

        widget-table-wrapper-fee-value:
            aligned horizontally bottom widget-table-wrapper-fee
            right-of widget-table-wrapper-fee-currency 5 to 10 px
            css font-size is "18px"
            css font-family is "Gotham-Medium"

        widget-table-wrapper-zero-fee:
            below widget-table-wrapper-fee 5 to 15 px
            aligned vertically left widget-table-wrapper-fee
            css font-size is "18px"
            css font-family is "Gotham-Medium"

        widget-table-wrapper-zero-fee-currency:
            aligned horizontally bottom widget-table-wrapper-zero-fee
            aligned vertically right widget-table-wrapper-fee
            css font-size is "18px"
            css font-family is "Gotham-Medium"

        widget-table-wrapper-zero-fee-value:
            aligned horizontally bottom widget-table-wrapper-zero-fee
            aligned vertically left widget-table-wrapper-fee-value
            css font-size is "18px"
            css font-family is "Gotham-Medium"

        widget-table-wrapper-total:
            below widget-table-wrapper-zero-fee 5 to 15 px
            aligned vertically left widget-table-wrapper-fee
            css font-size is "18px"
            css font-family is "Gotham-Medium"

        widget-table-wrapper-total-currency:
            aligned horizontally bottom widget-table-wrapper-total
            aligned vertically right widget-table-wrapper-fee
            css font-size is "18px"
            css font-family is "Gotham-Medium"

        widget-table-wrapper-total-value:
            aligned horizontally bottom widget-table-wrapper-total
            aligned vertically left widget-table-wrapper-fee-value
            css font-size is "18px"
            css font-family is "Gotham-Medium"

# Button
        widgetbutton-get-started:
            aligned vertically all widget
            below widget-table-wrapper 5 to 15 px
            height 80 to 90 px
            contains widgetbutton-get-started-*

        widgetbutton-get-started-text:
            centered all inside widgetbutton-get-started 1 px
            css font-size is "18px"
            css font-family is "Gotham-Medium"



































    @on Galaxy S5, Nexus 5X, Iphone 6 Plus
        widget:
            below header-banner-subheader-2 0 to 20 px
            centered horizontally inside screen 1 px


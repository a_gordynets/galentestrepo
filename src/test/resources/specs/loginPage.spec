@objects
    login-form                                   xpath    //*[@id='cboxWrapper']
    login-form-already-signet-text               xpath    //*[@class='col-xs-12']/h3
    login-form-not-yet-button                    xpath    //*[@class='js-sign-up btn btn-default']
    login-form-yes-button                        xpath    //*[@class='js-login btn btn-ghost-blue']
    login-form-secure-icon                       xpath    //*[@class='secure-ico']
    login-form-secure-fca-text                   xpath    //*[@class='col-xs-12']/p


= Login Form =
    @on *
        login-form:
            contains login-form-*
            visible

        login-form-already-signet-text:
            visible

        login-form-not-yet-button:
            visible

        login-form-yes-button:
            visible

        login-form-secure-icon:
            visible

        login-form-secure-fca-text:
            visible


    @on Galaxy S5, Nexus 5X, Iphone 6 Plus:
        login-form:
            contains login-form-*
            visible

        login-form-already-signet-text:
            visible

        login-form-not-yet-button:
            visible

        login-form-yes-button:
            visible

        login-form-secure-icon:
            visible

        login-form-secure-fca-text:
            visible






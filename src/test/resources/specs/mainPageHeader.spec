
@objects
    header                                    xpath     //*[@class='main-header']
    header-navbar                             xpath     //*[@class='navbar-toggle']
    header-site-logo                          xpath     //*[@class='site-logo-responsive row']
    header-banner-text                        xpath     //*[@class='banner-header']/h1
    header-banner-subheader-1                 xpath     //*[@class='banner-subheader']/p[1]
    header-banner-subheader-2                 xpath     //*[@class='banner-subheader']/p[2]




= Header (chrome) =
    @on *
        header:
            inside screen 0 px top
            centered horizontally inside screen 1 px

        header-banner-text:
            text is "Take control of your money transfer"

        header-banner-subheader-1:
            text is "Lock any great rate you see for up to 30 days. Or send money abroad now. You're in control."

        header-banner-subheader-2:
            text is "Get £10 off and pay zero fees on your first transfer, when you send £100 or more."


    @on desktop
        header:
            height ~ 48
            width 999 px

        header-site-logo:
            absent

        header-navbar:
            absent

        header-banner-text:
            css font-size is "26px"

        header-banner-subheader-1, header-banner-subheader-2:
            css font-size is "20px"


    @on Galaxy S5
        header:
            height ~ 55
            contains header-site-logo

        header-site-logo:
            height 50px
            width 94px
            inside header 25 to 35 px left, 0 to 5 px top

        header-navbar:
            inside header 25 to 35 px right, 15 to 25 px top

        header-banner-text:
            css font-size is "30px"
            centered horizontally inside screen 1 px
            below header 20 to 30 px

        header-banner-subheader-1:
            centered horizontally inside screen 1 px
            below header-banner-text 10 to 20 px

        header-banner-subheader-2:
            centered horizontally inside screen 1 px
            below header-banner-subheader-1 10 to 20 px


    @on Nexus 5X
        header:
            height ~ 55
            contains header-site-logo

        header-site-logo:
            height 50px
            width 94px
            inside header 25 to 35 px left, 0 to 5 px top

        header-banner-text:
            css font-size is "30px"
            centered horizontally inside screen 1 px
            below header 20 to 30 px

        header-navbar:
            inside header 25 to 35 px right, 15 to 25 px top

        header-banner-subheader-1:
            centered horizontally inside screen 1 px
            below header-banner-text 10 to 20 px

        header-banner-subheader-2:
            centered horizontally inside screen 1 px
            below header-banner-subheader-1 10 to 20 px


    @on Iphone 6 Plus
        header:
            height ~ 55
            contains header-site-logo

        header-site-logo:
            height 50px
            width 94px
            inside header 25 to 35 px left, 0 to 5 px top

        header-banner-text:
            css font-size is "30px"
            centered horizontally inside screen 1 px
            below header 20 to 30 px

        header-navbar:
            inside header 25 to 35 px right, 15 to 25 px top

        header-banner-subheader-1:
            centered horizontally inside screen 1 px
            below header-banner-text 10 to 20 px

        header-banner-subheader-2:
            centered horizontally inside screen 1 px
            below header-banner-subheader-1 10 to 20 px


    @on Ipad Pro
        header:
            height ~ 48

        header-site-logo:
            absent

        header-banner-text:
            css font-size is "26px"

        header-navbar:
            absent

        header-banner-subheader-1, header-banner-subheader-2:
            css font-size is "14px"





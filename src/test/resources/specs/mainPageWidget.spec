@objects

    widget                                    xpath     //*[@id='widget']
    widget-header                             xpath     //*[@class='widget-header']
    widget-header-text-span-1                 xpath     //*[@class='widget-header']//span[@class='fxRateContent']
    widget-header-text-span-2                 xpath     //*[@class='widget-header']//span[@class='fxRateContent fx-display']
    widget-header-text-span-2                 xpath     //*[@class='widget-header']//span[@class='fxRateContent fx-currency']
    widget-body                               xpath     //*[@class='widget-body']
    widget-body-transfer-field                xpath     //*[@class='transfer-fields']
    widget-body-you-send                      xpath     //*[@class='widget-body']//*[@for='send']
    widget-body-you-send-number               xpath     //*[@class='widget-body']//*[@id='send']
    widget-body-you-send-flag                 xpath     //*[@class='send']//img
    widget-body-you-send-sell-currency        xpath     //*[@class='send']//*[@id='sellCurrency']
    widget-body-switch                        xpath     //*[@class='switch']
    widget-body-they-get                      xpath     //*[@class='widget-body']//*[@for='receive']
    widget-body-they-get-number               xpath     //*[@class='widget-body']//*[@id='receive']
    widget-body-they-get-flag                 xpath     //*[@class='widget-body']//img
    widget-body-they-get-buy-country          xpath     //*[@class='widget-body']//*[@id='country-display']
    widget-body-they-get-buy-country-arrow    xpath     //*[@class='widget-body']//*[@class='arrow']
    widget-body-send-money                    xpath     //*[@class='widget-body']//p[@class='label no-padding-left']
    widget-body-send-money-today              xpath     //*[@class='widget-body']//*[@for='today']
    widget-body-send-money-later-label        xpath     //*[@class='widget-body']//*[@for='later']
    widget-body-send-money-later-info         xpath     //*[@class='widget-body']//*[@class='info']
    widget-body-send-money-later-info-pink    xpath     //*[@class='widget-body']//*[@class='text-pink lock']
    widget-table-wrapper                      xpath     //*[@class='table-wrapper']
    widget-table-wrapper-fee                  xpath     //*[@class='table-wrapper']//*[@id='fee-heading']
    widget-table-wrapper-fee-currency         xpath     //*[@class='table-wrapper']//*[@class='fee']/span
    widget-table-wrapper-fee-value            xpath     //*[@class='table-wrapper']//*[@class='fee']/p
    widget-table-wrapper-total                xpath     //*[@class='table-wrapper']//td[contains(text(),'Total')]
    widget-table-wrapper-total-currency       xpath     //*[@class='table-wrapper']//*[@class='total']/span
    widget-table-wrapper-total-value          xpath     //*[@class='table-wrapper']//*[@class='total']/p
    widgetbutton-get-started                  xpath     //*[@id='getStarted']
    widgetbutton-get-started-text             xpath     //*[@id='getStarted']/*[@id='content']

    header-banner-subheader-2                 xpath     //*[@class='banner-subheader']/p[2]


= Widget (chrome) =

    @on *
        widget:
            contains widget-*
            visible

        widget-*:
            visible

        widgetbutton-*:
            visible



    @on Galaxy S5, Nexus 5X, Iphone 6 Plus
        widget:
            below header-banner-subheader-2 0 to 20 px
            centered horizontally inside screen 1 px

